const gulp = require('gulp')
const sass = require('gulp-sass')
const babel = require('gulp-babel')
const uglify = require('gulp-uglify')
const changed = require('gulp-changed')
const imagemin = require('gulp-imagemin')
const jsImport = require('gulp-js-import')
const sourcemaps = require('gulp-sourcemaps')
const autoprefixer = require('gulp-autoprefixer')
const browserSync = require('browser-sync').create()

// Tasks definitions.
const sassTask = function(src, dist) {
  gulp.src([src])
    .pipe(sourcemaps.init())
    .pipe(sass(sass_opts).on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(dist))
    .pipe(browserSync.stream())
}

const scriptsTask = function(src, dist) {
  gulp.src(src)
    .pipe(jsImport({hideConsole: false}).on('error',
      (e) => (console.log(`[ERROR Import JS] ${e.message}`))))
    .pipe(babel({presets: ['@babel/preset-env']}))
    .pipe(uglify(uglify_opts))
    .pipe(gulp.dest(dist))
    .pipe(browserSync.stream())
}

const imageOptimizeTask = function(src, dist) {
  gulp.src(src)
    .pipe(changed(src))
    .pipe(imagemin())
    .pipe(gulp.dest(dist))
}

// Tasks options.
const uglify_opts = {
  mangle: false,
  compress: {
    unused: false
  }
}

const sass_opts = {outputStyle: 'compressed'}

gulp.task('sass', function (resolve) {
  sassTask(`./assets/sass/app.scss`, `./dist/css`)
  resolve();
})

gulp.task('js', function (resolve) {
  scriptsTask(['node_modules/babel-polyfill/dist/polyfill.js', './assets/js/app.js'], `./dist/js`)
  resolve()
});

gulp.task('image-optimize', function (resolve) {
  let src = `./assets/img/*.+(svg|png|jpg|jpeg|gif)`,
    dist = `./dist/img`

  imageOptimizeTask(src, dist)
  resolve()
});

gulp.task('watch', function () {
  gulp.watch(['./assets/sass/**/*.scss', './assets/sass/styles.scss'], gulp.parallel('sass', 'lint'))
  gulp.watch([`./assets/js/**/*.js`], gulp.parallel('js'))
});


gulp.task('lint', function lintCssTask() {
    const gulpStylelint = require('gulp-stylelint');

    return gulp
        .src('assets/sass/**/*.scss')
        .pipe(gulpStylelint({
            reporters: [
                {formatter: 'string', console: true}
            ]
        }));
});

gulp.task('serve', function() {
  browserSync.init({
    proxy: {
      target: "http://grav-portfolio.test//",
    }
  })

  gulp.watch(['./assets/sass/**/*.scss', './assets/sass/styles.scss'], gulp.parallel('sass', 'lint'))
  gulp.watch([`./assets/js/**/*.js`], gulp.parallel('js'))

})
gulp.task('default', gulp.series('serve'), function () {})
